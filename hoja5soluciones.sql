﻿USE jardineria;

-- 1. Escribe un procedimiento que reciba el nombre de un país como parámetro de entrada y
-- realice una consulta sobre la tabla cliente para obtener todos los clientes que existen en
-- la tabla de ese país.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo1(npais varchar(50))
    BEGIN

     SELECT c.codigo_cliente, c.nombre_contacto, c.apellido_contacto 
        FROM cliente c 
        WHERE c.pais = npais;

    END //
  DELIMITER ;

  CALL ejemplo1('Spain');

-- 2. Realizar una función que me devuelva el número de registros de la tabla clientes que
-- pertenezcan a un país que le pase por teclado.

  DELIMITER //
  CREATE OR REPLACE FUNCTION ejemplo2(npais varchar(50))
    RETURNS int
    BEGIN
      
      DECLARE vnumClientes int DEFAULT 0;

      SET vnumClientes = (
        SELECT COUNT(*) 
          FROM cliente c 
            WHERE c.pais = npais);  

      RETURN vnumClientes;
    END //
  DELIMITER ;

  SELECT ejemplo2('USA');

-- 3. Realizar el mismo ejercicio anterior, pero para desplazarme por la tabla utilizar
-- cursores con excepciones

-- 4. Escribe una función que le pasas una cantidad y que te devuelva el número total de
-- productos que hay en la tabla productos cuya cantidad en stock es superior a la
-- pasada.

  DELIMITER //
  CREATE OR REPLACE FUNCTION ejemplo4(cantidad int)
    RETURNS int
    BEGIN
      
      DECLARE vnumProductos int DEFAULT 0;

      SET vnumProductos = (
        SELECT COUNT(*) 
          FROM producto p 
          WHERE p.cantidad_en_stock > cantidad);

      RETURN vnumProductos;
    END //
  DELIMITER ;

  SELECT ejemplo4(150);

-- 5. Realizar el mismo ejercicio anterior pero con cursores

-- 6. Escribe un procedimiento que reciba como parámetro de entrada una forma de pago,
-- que será una cadena de caracteres (Ejemplo: PayPal, Transferencia, etc). Y devuelva
-- como salida el pago de máximo valor realizado para esa forma de pago. Deberá hacer
-- uso de la tabla pago de la base de datos jardinería.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo6(formaPago varchar(40))
    BEGIN
      
      DECLARE vmaximo int DEFAULT 0;

      SET vmaximo = (
        SELECT MAX(p.total) 
          FROM pago p 
          WHERE p.forma_pago = formaPago);

      SELECT vmaximo;

    END //
  DELIMITER ;

  CALL ejemplo6('paypal');

-- 7. Escribe un procedimiento que reciba como parámetro de entrada una forma de pago,
-- que será una cadena de caracteres (Ejemplo: PayPal, Transferencia, etc). Y devuelva
-- como salida los siguientes valores teniendo en cuenta la forma de pago seleccionada
-- como parámetro de entrada:
-- - el pago de máximo valor,
-- - el pago de mínimo valor,
-- - el valor medio de los pagos realizados,
-- - la suma de todos los pagos,
-- - el número de pagos realizados para esa forma de pago

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo7(formaPago varchar(40))
    BEGIN
      
      DECLARE vmaximo int DEFAULT 0;
      DECLARE vminimo int DEFAULT 0;
      DECLARE vmedia float DEFAULT 0;
      DECLARE vsuma int DEFAULT 0;
      DECLARE vnumPagos int DEFAULT 0;

      SET vmaximo = (
        SELECT MAX(p.total) 
          FROM pago p 
          WHERE p.forma_pago = formaPago);

      SET vminimo = (
        SELECT MIN(p.total) 
          FROM pago p 
          WHERE p.forma_pago = formaPago);

      SET vmedia = (
        SELECT AVG(p.total) 
          FROM pago p 
          WHERE p.forma_pago = formaPago);

      SET vsuma = (
        SELECT SUM(p.total) 
          FROM pago p 
          WHERE p.forma_pago = formaPago);

      SET vnumPagos = (
        SELECT COUNT(*) 
          FROM pago p 
          WHERE p.forma_pago = formaPago);

      SELECT vmaximo, vminimo, vmedia, vsuma, vnumPagos;
    END //
  DELIMITER ;

  CALL ejemplo7('paypal');
  CALL ejemplo7('transferencia');

-- 8. Escribe una función para la tabla productos que devuelva el valor medio del precio de
-- venta de los productos de un determinado proveedor que se recibirá como parámetro de
-- entrada. El parámetro de entrada será el nombre del proveedor

  DELIMITER //
  CREATE OR REPLACE FUNCTION ejemplo8(prove varchar(50))
    RETURNS float
    BEGIN
      
      DECLARE vmedia float DEFAULT 0;

      SET vmedia = (
        SELECT AVG(p.precio_venta) 
          FROM producto p 
          WHERE p.proveedor = prove);

      RETURN vmedia;
    END //
  DELIMITER ; 

  SELECT ejemplo8('Frutales Talavera S.A');

-- 9. Realizar el mismo ejercicio anterior, pero haciendo que en caso de que el fabricante
-- pasado no tenga productos produzca una excepción personalizada que muestre el error
-- fabricante no existe

  -- MODIFICAR PARA QUE FUNCIONE CON UNA EXCEPCION
  DELIMITER //
  CREATE OR REPLACE FUNCTION ejemplo9(prove varchar(50))
    RETURNS varchar(50)
    BEGIN
      
      DECLARE vmensaje varchar(50) DEFAULT 'Fabricante no existe';

      IF ((SELECT COUNT(*) FROM producto p WHERE p.proveedor= prove)<>0) THEN
        SET vmensaje = CONCAT('El valor medio es: ',(
        SELECT AVG(p.precio_venta) 
          FROM producto p 
          WHERE p.proveedor = prove));
      END IF;

      RETURN vmensaje;
    END //
  DELIMITER ;

  SELECT ejemplo9('Frutales Talavera S.A');
  SELECT ejemplo9('Mercadona');